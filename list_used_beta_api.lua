local getopt = require("getopt")

local eou = require("utils.eolian_extras")

local exit = false
local pargs

getopt.parse {
    usage = "Usage: %prog [OPTIONS] path_to_project [path_to_eo_files]",
    args = arg,
    header = [[
Print all beta C names used in the given project. You can specify a custom
path to .eo files as the second argument. If you don't, system-wide eo files
will be used instead.]],
    descs = {
        { "h", "help", false, help = "Show this message.",
            callback = getopt.help_cb(io.stdout)
        }
    },
    error_cb = function(parser, msg)
        io.stderr:write(msg, "\n")
        getopt.help(parser, io.stderr)
        exit = true
    end,
    done_cb = function(parser, opts, args)
        if opts["h"] then
            exit = true
        end
        if #args == 0 then
            parser:error_cb("project path not specified")
            return
        end
        pargs = args
    end
}

if exit then
    return true
end

local eos = eou.new()

if #pargs > 1 then
    eos:directory_add(pargs[2])
else
    eos:system_directory_add()
end

-- find all source, header and inline files first, we don't have a reasonable
-- filesystem api available here so this hack is necessary, maybe replace later
local f = io.popen(("find '%s' -name '*.[chx]'"):format(
    pargs[1]:gsub("'", "\\'")))
if not f then
    error("could not find source files")
end

local srcs = {}

-- a list of hardcoded names not in .eo files that we know of
local syms = {
    efl_add = true,
    efl_add_ref = true,
    efl_callbacks_cmp = true,
    efl_cast = true,
    efl_class_functions_set = true,
    efl_class_get = true,
    efl_class_name_get = true,
    efl_class_new = true,
    efl_compatible = true,
    efl_data_ref = true,
    efl_data_scope_get = true,
    efl_data_scope_safe_get = true,
    efl_data_unref = true,
    efl_data_xref = true,
    efl_data_xunref = true,
    efl_dbg_info_free = true,
    efl_dbg_info_get = true,
    efl_debug_name_get = true,
    efl_del = true,
    efl_del_intercept_get = true,
    efl_del_intercept_set = true,
    efl_destructed_is = true,
    efl_domain_current_get = true,
    efl_domain_current_pop = true,
    efl_domain_current_push = true,
    efl_domain_current_set = true,
    efl_domain_data_adopt = true,
    efl_domain_data_get = true,
    efl_domain_data_return = true,
    efl_domain_get = true,
    efl_domain_switch = true,
    efl_event_callback_add = true,
    efl_event_callback_array_add = true,
    efl_event_callback_array_del = true,
    efl_event_callback_array_priority_add = true,
    efl_event_callback_call = true,
    efl_event_callback_del = true,
    efl_event_callback_legacy_call = true,
    efl_event_callback_priority_add = true,
    efl_future_Eina_FutureXXX_then = true,
    efl_future_cb = true,
    efl_future_cb_from_desc = true,
    efl_future_chain = true,
    efl_future_chain_array = true,
    efl_isa = true,
    efl_key_data_get = true,
    efl_key_data_set = true,
    efl_key_ref_get = true,
    efl_key_ref_set = true,
    efl_key_value_get = true,
    efl_key_value_set = true,
    efl_key_wref_get = true,
    efl_key_wref_set = true,
    efl_manual_free = true,
    efl_manual_free_set = true,
    efl_new = true,
    efl_object_init = true,
    efl_object_legacy_only_event_description_get = true,
    efl_object_override = true,
    efl_object_shutdown = true,
    efl_ref = true,
    efl_ref_count = true,
    efl_reuse = true,
    efl_super = true,
    efl_unref = true,
    efl_weak_ref = true,
    efl_weak_unref = true,
    efl_wref_add = true,
    efl_wref_del = true,
    efl_wref_del_safe = true,
    efl_xref = true,
    efl_xunref = true
}

-- collect source files
for fn in f:lines() do
    srcs[#srcs + 1] = fn
end
f:close()

-- collect names as a lookup table
eos:all_eot_files_parse()
eos:all_eo_files_parse()

local add_syms = function(fun)
    for i, n in ipairs(fun(eos)) do
        syms[n] = true
    end
end

add_syms(eou.get_c_functions)
add_syms(eou.get_c_events)
add_syms(eou.get_c_function_pointers)
--add_syms(eou.get_c_aliases)
--add_syms(eou.get_c_structs)
--add_syms(eou.get_c_enums)
add_syms(eou.get_c_globals)
add_syms(eou.get_c_constants)

-- match words in sources against names

local match_names = function(fn)
    local fs = io.open(fn)
    if not fs then
        return
    end
    local lnum = 1
    for l in fs:lines() do
        -- C names on thel ine
        for w in l:gmatch("[%a_][%w_]*") do
            if syms[w] then
                print(("%s:%d: used beta name '%s'"):format(fn, lnum, w))
            end
        end
        lnum = lnum + 1
    end
    fs:close()
end

for i, fn in ipairs(srcs) do
    match_names(fn)
end

return true
