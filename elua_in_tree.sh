#!/bin/sh

if [ $# -lt 2 ]; then
    echo "Usage: $0 efl_path script_path [arguments...]"
    exit 1
fi

EFL_PATH="$1"
shift

PREV_LD_PATH="$LD_LIBRARY_PATH"

export LD_LIBRARY_PATH="${EFL_PATH}/src/lib/eina/.libs"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${EFL_PATH}/src/lib/eolian/.libs"

if [ -n "$PREV_LD_PATH" ]; then
    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${PREV_LD_PATH}"
fi

"${EFL_PATH}/src/bin/elua/elua" \
    "-I${EFL_PATH}/src/bindings/luajit" \
    "-C${EFL_PATH}/src/scripts/elua/core" \
    "-M${EFL_PATH}/src/scripts/elua/modules" \
    "-A${EFL_PATH}/src/scripts/elua/apps" \
    "$@"
