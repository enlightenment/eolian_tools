local eolian = require("eolian")

local M = {}

-- check for pre-state eolian api
local legacy = not eolian.new

-- wrapper for legacy eolian api to mimic modern
local legacy_eos = {
    directory_add = function(self, dir)
        eolian.directory_scan(dir)
    end,

    system_directory_add = function(self)
        eolian.system_directory_scan()
    end,

    all_eo_files_parse = function(self)
        assert(eolian.all_eo_files_parse())
        assert(eolian.database_validate())
    end,

    all_eot_files_parse = function(self)
        assert(eolian.all_eot_files_parse())
        assert(eolian.database_validate())
    end,

    classes_get = function(self)
        return eolian.all_classes_get(nil)
    end,

    aliases_get = function(self)
        return eolian.typedecl_all_aliases_get(nil)
    end,

    structs_get = function(self)
        return eolian.typedecl_all_structs_get(nil)
    end,

    enums_get = function(self)
        return eolian.typedecl_all_enums_get(nil)
    end,

    globals_get = function(self)
        return eolian.variable_all_globals_get(nil)
    end,

    constants_get = function(self)
        return eolian.variable_all_constants_get(nil)
    end
}

M.new = function()
    if legacy then
        return legacy_eos
    end
    return eolian.new()
end

local f_is_beta = function(fn)
    if legacy then
        return false
    end
    return fn:is_beta()
end

M.get_c_functions = function(eos, only_legacy)
    only_legacy = not not only_legacy
    local ret = {}
    for cl in eos:classes_get() do
        local fntp = eolian.function_type
        for fn in cl:functions_get(fntp.METHOD) do
            if not only_legacy or not f_is_beta(fn) then
                ret[#ret + 1] = fn:full_c_name_get(fntp.METHOD, only_legacy)
            end
        end
        for fn in cl:functions_get(fntp.PROPERTY) do
            if not only_legacy or not f_is_beta(fn) then
                local tp = fn:type_get()
                if tp == fntp.PROPERTY then
                    ret[#ret + 1] = fn:full_c_name_get(fntp.PROP_GET, only_legacy)
                    ret[#ret + 1] = fn:full_c_name_get(fntp.PROP_SET, only_legacy)
                else
                    ret[#ret + 1] = fn:full_c_name_get(tp, only_legacy)
                end
            end
        end
    end
    return ret
end

M.get_c_events = function(eos, only_legacy)
    local ret = {}
    if only_legacy then
        return ret
    end
    for cl in eos:classes_get() do
        for ev in cl:events_get() do
            ret[#ret + 1] = ev:c_name_get()
        end
    end
    return ret
end

local full_name_get = function(obj)
    if legacy then
        return obj:full_name_get()
    end
    return obj:name_get()
end

M.get_c_function_pointers = function(eos, only_legacy)
    if legacy or only_legacy then
        -- no function pointer support in pre-state api
        return {}
    end
    local ret = {}
    for tp in eos:aliases_get() do
        if tp:type_get() == eolian.typedecl_type.FUNCTION_POINTER then
            ret[#ret + 1] = full_name_get(tp):gsub("%.", "_")
        end
    end
    return ret
end

M.get_c_aliases = function(eos)
    local ret = {}
    for tp in eos:aliases_get() do
        if tp:type_get() ~= eolian.typedecl_type.FUNCTION_POINTER then
            ret[#ret + 1] = full_name_get(tp):gsub("%.", "_")
        end
    end
    return ret
end

M.get_c_structs = function(eos)
    local ret = {}
    for tp in eos:structs_get() do
        ret[#ret + 1] = full_name_get(tp):gsub("%.", "_")
    end
    return ret
end

M.get_c_enums = function(eos)
    local ret = {}
    for tp in eos:enums_get() do
        ret[#ret + 1] = full_name_get(tp):gsub("%.", "_")
    end
    return ret
end

M.get_c_globals = function(eos)
    local ret = {}
    for v in eos:globals_get() do
        ret[#ret + 1] = full_name_get(v):gsub("%.", "_"):upper()
    end
    return ret
end

M.get_c_constants = function(eos)
    local ret = {}
    for v in eos:constants_get() do
        ret[#ret + 1] = full_name_get(v):gsub("%.", "_"):upper()
    end
    return ret
end

return M
