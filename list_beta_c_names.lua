local getopt = require("getopt")

local eou = require("utils.eolian_extras")

local exit = false
local legacy = false
local pargs

getopt.parse {
    usage = "Usage: %prog [OPTIONS] [path_to_eo_files]",
    args = arg,
    header = [[
Print all non-legacy C names generated in header files for the given
path. If no path is specified, system-wide eo files are used instead.]],
    descs = {
        { "h", "help", false, help = "Show this message.",
            callback = getopt.help_cb(io.stdout)
        },
        { "l", "legacy", false, help = "Print only legacy names intead." }
    },
    error_cb = function(parser, msg)
        io.stderr:write(msg, "\n")
        getopt.help(parser, io.stderr)
        exit = true
    end,
    done_cb = function(parser, opts, args)
        if opts["h"] then
            exit = true
        end
        if opts["l"] then
            legacy = true
        end
        pargs = args
    end
}

if exit then
    return true
end

local eos = eou.new()

if #pargs >= 1 then
    eos:directory_add(pargs[1])
else
    eos:system_directory_add()
end

eos:all_eot_files_parse()
eos:all_eo_files_parse()

print("API functions:\n")
for i, n in ipairs(eou.get_c_functions(eos, legacy)) do
    print(n)
end

print("\nEvents:\n")
for i, n in ipairs(eou.get_c_events(eos, legacy)) do
    print(n)
end

print("\nFunction pointers:\n")
for i, n in ipairs(eou.get_c_function_pointers(eos, legacy)) do
    print(n)
end

print("\nAliases:\n")
for i, n in ipairs(eou.get_c_aliases(eos)) do
    print(n)
end

print("\nStructs:\n")
for i, n in ipairs(eou.get_c_structs(eos)) do
    print(n)
end

print("\nEnums:\n")
for i, n in ipairs(eou.get_c_enums(eos)) do
    print(n)
end

print("\nGlobals:\n")
for i, n in ipairs(eou.get_c_globals(eos)) do
    print(n)
end

print("\nConstants:\n")
for i, n in ipairs(eou.get_c_constants(eos)) do
    print(n)
end

return true
